const Joi = require("@hapi/joi");
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    title: {type: String, required: true, trim: true, minlength: 1},
    description: {type: String, required: true, trim: true, minlength: 1},
    username: {type: String, required: true, trim: true, minlength: 1},
    votes: {type: Number, default: 0},
    time: {type: Date, default: Date.now}
});

function validateItem(talk) {
    const schema = {
        title: Joi.string().min(1).required(),
        description: Joi.string().min(1).required(),
        username: Joi.string().min(1).required()
    };
    return Joi.validate(talk, schema);
}

exports.Talk = mongoose.model('Talk', schema);
exports.validate = validateItem;
