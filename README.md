# lightning-talks

Here is a online demo for this project [https://3dprintingsystems.w-k.io/](https://3dprintingsystems.w-k.io/)

## How To Run It
1.  go to root folder
2.  yarn start or npm start
3.  cd client
4.  yarn start or npm start
5.  open http://localhost:3001/

## APIs
*  List of talks
    * GET /api/talk
    * `[{
    username: "Kevin Wang",
    title: "Full-Stack Developer",
    description: "3dprintingsystems Full-stack Developer test"
},
{
    username: "Linus Torvalds",
    title: "Software Engineer",
    description: "Talk is cheap. Show me the code."
}]`
*  Post new talk
    * POST /api/talk
    * `{
    username: "Kevin Wang",
    title: "Full-Stack Developer",
    description: "3dprintingsystems Full-stack Developer test"
}`
*  Vote a talk
    * GET /api/talk/{id}/vote