const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');

const indexRouter = require('./routes/index');
const talkRouter = require('./routes/talk');

const app = express();

mongoose.connect('mongodb://localhost:27017/lightning-talks', {useNewUrlParser: true})
    .then(() => console.log('Now connected to MongoDB!'))
    .catch(err => console.error('Something went wrong', err));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client')));

// app.use('/', indexRouter);
app.use('/api/talk', talkRouter);

module.exports = app;
