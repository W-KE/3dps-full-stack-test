let actions = {
    voteTalk: (talkId) => {
        return (dispatch, getState) => {
            for (let talk in getState().talkList) {
                if (talkId.equals(talk.id)) {
                    return;
                }
            }
            fetch('/api/talk/' + talkId + '/vote')
                .then(res => {
                    dispatch({type: "TALK/VOTE"});
                    return dispatch(actions.topTalk())
                })
        }
    },
    submitTalk: (talk) => {
        return (dispatch) => {
            fetch('/api/talk', {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(talk)
            })
                .then(dispatch({type: "TALK/SUBMIT"}))
                .then(dispatch(actions.topTalk()))
        }
    },
    topTalk: () => (dispatch) => {
        fetch('/api/talk')
            .then(res => res.json())
            .then(json => dispatch({
                type: 'TALK/TOP',
                payload: json
            }))
    }
};

export default actions;