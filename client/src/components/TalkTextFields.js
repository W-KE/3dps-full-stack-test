import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import {bindActionCreators} from "redux";
import actions from "../actions/talkActions";
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
    button: {
        margin: "auto",
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    }
});

class TalkTextFields extends React.Component {
    handleChange = (event) => {
        switch (event.target.id) {
            case 'username':
                this.props.talk.username = event.target.value;
                break;
            case 'title':
                this.props.talk.title = event.target.value;
                break;
            case 'description':
                this.props.talk.description = event.target.value;
                break;
            default:
                break;
        }
    };

    handleClick = () => {
        this.props.actions.submitTalk(this.props.talk);
    };

    render() {
        return (
            <form className={this.props.classes.container} noValidate autoComplete="off">
                <TextField
                    id="username"
                    label="Name"
                    style={{margin: 8}}
                    placeholder="Name"
                    fullWidth
                    onChange={this.handleChange}
                    margin="normal"
                    variant="outlined"
                />
                <TextField
                    id="title"
                    label="Title"
                    style={{margin: 8}}
                    placeholder="Title"
                    fullWidth
                    onChange={this.handleChange}
                    margin="normal"
                    variant="outlined"
                />
                <TextField
                    id="description"
                    label="Description"
                    style={{margin: 8}}
                    placeholder="Description"
                    fullWidth
                    onChange={this.handleChange}
                    margin="normal"
                    variant="outlined"
                />
                <Button variant="contained" color="primary" className={this.props.classes.button}
                        onClick={this.handleClick}>
                    Send
                    <Icon>send</Icon>
                </Button>
            </form>
        );
    }
}

TalkTextFields.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    talk: state.talkReducer.talk
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(TalkTextFields));