import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import actions from "../actions/talkActions";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import Icon from '@material-ui/core/Icon';

class TalkList extends React.Component {
    componentDidMount() {
        this.props.actions.topTalk();
    }

    render() {
        let welcome;
        if (this.props.talkList.length === 0) {
            welcome = <div style={{textAlign: 'center', padding: '1em', color: 'LightCoral'}}>"It's so empty here. Please say some thing."</div>
        }
        return (
            <Paper>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Title</TableCell>
                            <TableCell text-align="right">Description</TableCell>
                            <TableCell text-align="right">Author</TableCell>
                            <TableCell text-align="right">Time</TableCell>
                            <TableCell text-align="right">Votes</TableCell>
                            <TableCell text-align="right">Vote</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.talkList.map(talk => (
                            <TableRow key={talk._id}>
                                <TableCell component="th" scope="row">
                                    {talk.title}
                                </TableCell>
                                <TableCell text-align="right">{talk.description}</TableCell>
                                <TableCell text-align="right">{talk.username}</TableCell>
                                <TableCell text-align="right">{talk.time}</TableCell>
                                <TableCell text-align="right">{talk.votes}</TableCell>
                                <TableCell text-align="right">
                                    <IconButton edge="end" aria-label="vote"
                                                onClick={this.props.actions.voteTalk.bind(this, talk._id)}>
                                        <Icon>thumb_up</Icon>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                {welcome}
            </Paper>
        );
    }
}

const mapStateToProps = state => ({
    talkList: state.talkReducer.talkList
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(TalkList);