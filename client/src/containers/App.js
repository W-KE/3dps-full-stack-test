import React from "react";
import TalkList from '../components/TalkList';
import TalkTextFields from "../components/TalkTextFields";

export default class App extends React.Component {
    render() {
        return (
            <div className="container">
                <TalkList/>
                <TalkTextFields/>
            </div>
        );
    }
}
