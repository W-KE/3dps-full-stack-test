import { combineReducers } from 'redux';
import talkReducer from './talkReducer';

const reducer = combineReducers({
    talkReducer: talkReducer
});

export default reducer;