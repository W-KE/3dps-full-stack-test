const initialState = {
    talk: {
        username: "",
        title: "",
        description: ""
    },
    talkList: []
};


export default (state = initialState, action) => {
    switch (action.type) {
        case 'TALK/VOTE':
            state = {
                ...state,
            };
            break;
        case 'TALK/SUBMIT':
            state = {
                ...state,
                talk: {
                    username: "",
                    title: "",
                    description: ""
                }
            };
            break;
        case 'TALK/TOP':
            state = {
                ...state,
                talkList: action.payload
            };
            break;
        default:
            break
    }
    return state;
};