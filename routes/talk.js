const express = require('express');
const router = express.Router();
const ObjectId = require('mongoose').Types.ObjectId;
const {Talk, validate} = require('../models/talk');

/* GET talks listing */
router.get('/', async function (req, res, next) {
    await Talk.find({}).sort({votes: -1, time: -1}).exec((err, talks) => {
        if (err) {
            res.send(500, {error: err});
        } else {
            res.send(talks);
        }
    });
});

/* POST add new talk */
router.post('/', async function (req, res, next) {
    // First Validate The Request
    console.log(req.body);
    const {error} = validate(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    // Insert the new talk
    const item = new Talk({
        title: req.body.title,
        description: req.body.description,
        username: req.body.username,
        votes: 0
    });
    await item.save();
    await Talk.find({}).sort({votes: -1, time: -1}).exec((err, talks) => {
        if (err) {
            res.send(500, {error: err});
        } else {
            res.send(talks);
        }
    });
});

/* GET vote for talk */
router.get('/:id/vote', async function (req, res, next) {
    const id = req.params.id;
    await Talk.findOne({_id: new ObjectId(id)}).exec((err, talk) => {
        if (err) {
            res.send(500, {error: err});
        } else {
            talk.votes++;
            talk.save();
        }
    });
    await Talk.find({}).sort({votes: -1, time: -1}).exec((err, talks) => {
        if (err) {
            res.send(500, {error: err});
        } else {
            res.send(talks);
        }
    });
});

module.exports = router;
