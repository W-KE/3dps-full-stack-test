const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    res.send({ express: 'EXPRESS BACKEND IS CONNECTED TO REACT'});
});

module.exports = router;
